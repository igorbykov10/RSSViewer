package com.bykov.igor.rssviewer.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bykov.igor.rssviewer.R;
import com.bykov.igor.rssviewer.rss.RssItem;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class SingleRssAdapter extends ArrayAdapter<RssItem> {


    public SingleRssAdapter(Context context, ArrayList<RssItem> items){
        super(context, 0, items);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = LayoutInflater.from(getContext()).inflate(R.layout.business_layout_item, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }
        RssItem item = getItem(position);
        holder.name.setText(item.getTitle());
        return view;
    }

    public static class ViewHolder{
        @InjectView(R.id.tv_rss_name)
        TextView name;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

}
