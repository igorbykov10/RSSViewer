package com.bykov.igor.rssviewer.ui.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.bykov.igor.rssviewer.network.CallBack;
import com.bykov.igor.rssviewer.network.scheduled.Scheduled;
import com.bykov.igor.rssviewer.rss.RssItem;
import com.bykov.igor.rssviewer.ui.adapter.ComplexRssAdapter;
import com.bykov.igor.rssviewer.ui.model.RssModels;

import java.util.ArrayList;

public class EnvironmentEntertainmentNewsFragment extends BaseListFragment {

    private static final String TAG = EnvironmentEntertainmentNewsFragment.class.getSimpleName();

    private Scheduled mScheduledEnvironmentNews;
    private Scheduled mScheduledEntertainmentNews;

    private RssModels rssModels;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mScheduledEnvironmentNews = new Scheduled();
        mScheduledEntertainmentNews = new Scheduled();
        mScheduledEntertainmentNews.execute("http://feeds.reuters.com/reuters/entertainment",
                new CallBack<ArrayList<RssItem>>() {
                    @Override
                    public void success(ArrayList<RssItem> response) {
                        Log.d(TAG, "Update EntertainmentRss");
                        rssModels.setEntertainmentRss(response);
                        mRssAdapter.notifyDataSetChanged();
                        setListShown(true);
                    }

                    @Override
                    public void isExecuting() {
                    }

                    @Override
                    public void failure(String message) {
                        setListShown(true);
                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                    }
                });
        mScheduledEnvironmentNews.execute("http://feeds.reuters.com/reuters/environment",
                new CallBack<ArrayList<RssItem>>() {
                    @Override
                    public void success(ArrayList<RssItem> response) {
                        Log.d(TAG, "Update EnvironmentRss");
                        rssModels.setEnvironmentRss(response);
                        mRssAdapter.notifyDataSetChanged();
                        setListShown(true);
                    }

                    @Override
                    public void isExecuting() {
                        setListShown(false);
                    }

                    @Override
                    public void failure(String message) {
                        setListShown(true);
                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                    }
                });
    }

    @Override
    ArrayAdapter<RssItem> setupAdapter() {
        rssModels = new RssModels();
        return new ComplexRssAdapter(getActivity(), rssModels);
    }

    @Override
    public void onDestroyView() {
        mScheduledEntertainmentNews.shutdownNow();
        mScheduledEnvironmentNews.shutdownNow();
        super.onDestroyView();
    }


}
