package com.bykov.igor.rssviewer.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bykov.igor.rssviewer.R;
import com.bykov.igor.rssviewer.ui.activity.DetailActivity;

import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends BaseFragment {

    public static int REQUEST_CODE = 123;

    @InjectView(R.id.tv_time)
    TextView currentTimeView;

    @InjectView(R.id.tv_last_link)
    TextView lastLinkView;

    @OnClick(R.id.btn_rss_viewer)
    protected void onDetailActivity() {
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateTime();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE && data != null ) {
            String result = data.getStringExtra(DetailActivity.EXTRA_DATA);
            lastLinkView.setText(result);
        }
    }

    private void updateTime(){
        currentTimeView.setText(new LocalDateTime().toString());
    }
}
