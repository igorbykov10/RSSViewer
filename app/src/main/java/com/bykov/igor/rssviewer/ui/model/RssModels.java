package com.bykov.igor.rssviewer.ui.model;

import com.bykov.igor.rssviewer.rss.RssItem;

import java.util.ArrayList;

public class RssModels {

    private ArrayList<RssItem> environmentRss;
    private ArrayList<RssItem> entertainmentRss;

    public RssModels(){
        environmentRss = new ArrayList<>();
        entertainmentRss = new ArrayList<>();
    }

    public ArrayList<RssItem> getEnvironmentRss() {
        return environmentRss;
    }

    public void setEnvironmentRss(ArrayList<RssItem> environmentRss) {
        this.environmentRss = environmentRss;
    }

    public ArrayList<RssItem> getEntertainmentRss() {
        return entertainmentRss;
    }

    public void setEntertainmentRss(ArrayList<RssItem> entertainmentRss) {
        this.entertainmentRss = entertainmentRss;
    }
}
