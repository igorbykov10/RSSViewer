package com.bykov.igor.rssviewer.ui.fragment;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.bykov.igor.rssviewer.R;
import com.bykov.igor.rssviewer.rss.RssItem;
import com.bykov.igor.rssviewer.ui.activity.DetailActivity;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public abstract class BaseListFragment extends ListFragment {

    public static final String EXTRA_LAST_PRESSED = "last_pressed";

    protected ArrayAdapter<RssItem> mRssAdapter;
    private static Integer lastPressed;

    public interface onBackPressed{
        String backPressed();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof DetailActivity)
            ((DetailActivity)activity).setListenerOnBackPressed(new onBackPressed() {
                @Override
                public String backPressed() {
                    if (lastPressed != null)
                        return mRssAdapter.getItem(lastPressed).getTitle();
                    return null;
                }
            });
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRssAdapter = setupAdapter();
        setListAdapter(mRssAdapter);
        injectViews(view);
    }

    abstract ArrayAdapter<RssItem> setupAdapter();

    private void injectViews(View view) {
        ButterKnife.inject(this, view);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        lastPressed = position;
        RssItem item = (RssItem)getListAdapter().getItem(position);
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(item.getLink()));
        startActivity(i);
    }

}
