package com.bykov.igor.rssviewer.ui.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.bykov.igor.rssviewer.network.CallBack;
import com.bykov.igor.rssviewer.network.scheduled.Scheduled;
import com.bykov.igor.rssviewer.rss.RssItem;
import com.bykov.igor.rssviewer.ui.adapter.SingleRssAdapter;

import java.util.ArrayList;

public class BusinessNewsFragment extends BaseListFragment {

    private Scheduled mScheduled;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mScheduled = new Scheduled();
        mScheduled.execute("http://feeds.reuters.com/reuters/businessNews", new CallBack<ArrayList<RssItem>>() {
            @Override
            public void success(ArrayList<RssItem> response) {
                setListShown(true);
                mRssAdapter.setNotifyOnChange(false);
                mRssAdapter.clear();
                mRssAdapter.addAll(response);
                mRssAdapter.notifyDataSetChanged();
            }

            @Override
            public void isExecuting() {
                setListShown(false);
            }

            @Override
            public void failure(String message) {
                setListShown(true);
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    ArrayAdapter<RssItem> setupAdapter() {
        return new SingleRssAdapter(getActivity(), new ArrayList<RssItem>());
    }

    @Override
    public void onDestroyView() {
        mScheduled.shutdownNow();
        super.onDestroyView();
    }

}
