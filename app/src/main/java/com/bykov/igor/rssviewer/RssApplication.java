package com.bykov.igor.rssviewer;

import android.app.Application;
import android.content.Context;

public class RssApplication extends Application {

    private static RssApplication application;

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
    }

    public static RssApplication getInstance() {
        return application;
    }

}
