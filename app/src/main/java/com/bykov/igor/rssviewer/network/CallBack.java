package com.bykov.igor.rssviewer.network;

public interface CallBack<T> {

    void success(T response);

    void isExecuting();

    void failure(String message);
}
