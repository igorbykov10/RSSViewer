package com.bykov.igor.rssviewer.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import com.bykov.igor.rssviewer.R;
import com.bykov.igor.rssviewer.ui.adapter.TabsFragmentPagerAdapter;
import com.bykov.igor.rssviewer.ui.fragment.BaseListFragment;
import com.bykov.igor.rssviewer.ui.fragment.BusinessNewsFragment;
import com.bykov.igor.rssviewer.ui.fragment.EnvironmentEntertainmentNewsFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class DetailActivity extends Activity {

    public static final String EXTRA_DATA = "data";

    @InjectView(R.id.viewpager)
    ViewPager pager;
    @InjectView(R.id.sliding_tabs)
    TabLayout tabLayout;

    private BaseListFragment.onBackPressed mListenerOnBackPressed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_activity);
        ButterKnife.inject(this);
        pager.setAdapter(new TabsFragmentPagerAdapter(getFragmentManager(), setupPagerItems()));
        tabLayout.setupWithViewPager(pager);
    }

    private List<TabsFragmentPagerAdapter.PagerItem> setupPagerItems(){
        List<TabsFragmentPagerAdapter.PagerItem> pagerItems = new ArrayList<>(2);
        TabsFragmentPagerAdapter.PagerItem pagerItem1
                = new TabsFragmentPagerAdapter.PagerItem(BusinessNewsFragment.class, getString(R.string.business));
        TabsFragmentPagerAdapter.PagerItem pagerItem2
                = new TabsFragmentPagerAdapter.PagerItem(EnvironmentEntertainmentNewsFragment.class, getString(R.string.environment));
        pagerItems.add(pagerItem1);
        pagerItems.add(pagerItem2);
        return pagerItems;
    }

    public void setListenerOnBackPressed(BaseListFragment.onBackPressed listenerOnBackPressed) {
        this.mListenerOnBackPressed = listenerOnBackPressed;
    }

    @Override
    public void onBackPressed() {
        if (mListenerOnBackPressed != null) {
            String result = mListenerOnBackPressed.backPressed();
            if (result != null) {
                Intent intent = new Intent();
                intent.putExtra(EXTRA_DATA, result);
                setResult(RESULT_OK, intent);
                finish();
            }
        }
        super.onBackPressed();
    }

}
