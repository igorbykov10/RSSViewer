package com.bykov.igor.rssviewer.network.scheduled;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;

import com.bykov.igor.rssviewer.RssApplication;
import com.bykov.igor.rssviewer.network.CallBack;
import com.bykov.igor.rssviewer.rss.RssFeed;
import com.bykov.igor.rssviewer.rss.RssItem;
import com.bykov.igor.rssviewer.rss.RssReader;

import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Scheduled {

    private final ScheduledExecutorService worker =
            Executors.newSingleThreadScheduledExecutor();

    private Handler mHandler;

    public Scheduled(){
        mHandler = new Handler(Looper.myLooper());
    }

    public <T> void execute(String url, final CallBack<T> callBack){
        TimedFutureTask<T> timedFutureTask = new TimedFutureTask<>(url, callBack);
        if (isOnline())
            worker.scheduleAtFixedRate(timedFutureTask, 0, 5, TimeUnit.SECONDS);
    }

    public void shutdownNow(){
        worker.shutdownNow();
    }

    private class TimedFutureTask<T> implements Runnable {

        private String mUrl;
        private CallBack mCallBack;

        public TimedFutureTask(String url, CallBack<T> callBack){
            mUrl = url;
            mCallBack = callBack;
        }

        @Override
        public void run() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (!worker.isShutdown())
                        mCallBack.isExecuting();
                }
            });
            try {
                RssFeed rssReader = RssReader.read(new URL(mUrl));
                final ArrayList<RssItem> rssItems = rssReader.getRssItems();
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (!worker.isShutdown())
                            mCallBack.success(rssItems);
                    }
                });
            } catch (IOException | SAXException | IllegalStateException e) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (!worker.isShutdown())
                            mCallBack.failure(e.getMessage());
                    }
                });
                e.printStackTrace();
            }
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) RssApplication.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
